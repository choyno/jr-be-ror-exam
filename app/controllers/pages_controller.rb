class PagesController < ApplicationController
  def index; end

  def article_view; end

  def new; end

  def edit; end

  def sign_up; end

  def sign_in; end

  def edit_profile; end

  def my_news; end

  def sappl; end
end
