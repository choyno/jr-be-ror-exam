Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  root :to => 'pages#index'

  resources :pages

  #resources :pages, only: [:index]
  get 'sign-up', to: "pages#sign_up"
  get 'sign-in', to: "pages#sign_in"
  get 'news/article-1', to: "pages#article_view"
  get 'news/new', to: "pages#new"
  get 'news/edit', to: "pages#edit"
  get 'edit-profile', to: "pages#edit_profile"
  get 'my-news', to: "pages#my_news"
  get 'authors', to: "pages#edit"

end
